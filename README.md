# Dummy Model #

A dummy model for bitcoin price prediction using fbprophet model which is a tool for time series analysis and forecasting.

### Data ###

Data for this model is provided by [Bitcoinity](https://data.bitcoinity.org) which you can adjust data specifications and get a static csv link. 

### Hyperparameters ###
Future work on this model could be tuning these hyperparameters (and maybe more parameters):

* daily_seasonality
* yearly_seasonality
* weekly_seasonality                     
* seasonality_mode
* interval_width
* changepoint_prior_scale
* changepoint_range
